class Directory < ActiveRecord::Base
  validates_uniqueness_of :path

  class << self
    def scan
      Directory.scan_directory('/Users/Larry/Movies')
    end

    def get_film_files(path)
      files = []

      Dir.foreach(path) do |item|
        next if item == '.' or item == '..'

        item_path = "#{path}/#{item}"

        unless File.directory? item_path
          if File.extname(item_path).downcase =~ /rmvb|avi|mp4|mkv/
            files << item
          end
        end
      end

      files
    end

    def scan_directory(path)
      Dir.foreach(path) do |item|
        next if item == '.' or item == '..'

        item_path = "#{path}/#{item}"

        if File.directory? item_path
          Directory.scan_directory(item_path)
        else
          if File.extname(item_path).downcase =~ /rmvb|avi|mp4|mkv/
            logger.info "creating #{path}"
            Directory.create(:path => path)
          end
        end
      end
    end
  end
end
class DirectoriesController < ApplicationController
  def index
    @directories = Directory.all
  end

  def show
    @directory = Directory.find(params[:id])
    @film_files = Directory.get_film_files(@directory.path)
  end

  def scan
    Directory.scan
  end
end
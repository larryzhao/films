class CreateDirectories < ActiveRecord::Migration
  def change
    create_table :directories do |t|
      t.string :path, :null => false
      t.timestamps
    end
  end
end
